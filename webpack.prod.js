const merge = require('webpack-merge');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { BaseHrefWebpackPlugin } = require('base-href-webpack-plugin');

const common = require('./webpack.config.js');

module.exports = merge(common, {
    output: {
        publicPath: "/frontend/"
    },
    mode: 'production',
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',  // translates CSS into CommonJS
                    // "resolve-url-loader",
                    "postcss-loader",
                    "sass-loader" // compiles Sass to CSS, using Node Sass by default
                ]
            },
        ]
    },
    plugins: [
        new CleanWebpackPlugin([
            'dist'
        ]),
        // new HtmlWebpackPlugin(),         // We tried this for base and meta
        // new BaseHrefWebpackPlugin({ baseHref: '/frontend' }), // Didnt do what we wanted
        new MiniCssExtractPlugin({
            filename: '[name].[hash].css',
            chunkFilename: '[id].[hash].css',
        }),
        new OptimizeCSSAssetsPlugin({}),
        // new UglifyJSPlugin({
        //     sourceMap: true
        // })
    ]
});
