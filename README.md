# This is the frontend for SmartRaise

```yarn start```

## State
Currently test data is hardcoded, proper data structures, loading and dataflow will be ported over, once it's stable in admin.

## NPM Warnings

`npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.4 (node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.4: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})`

fsevents provides native access to Mac OS-X FSEvents, so only works on Mac OS-X.
 
`npm WARN awesome-typescript-loader@5.2.0 requires a peer of typescript@^2.7 but none is installed. You must install peer dependencies yourself.`

Works with typescript 3, despite warning.


----
Web3 package needs to be beta 34 as per this CORS issue

https://github.com/ethereum/web3.js/issues/1802
https://github.com/ethereum/web3.js/commit/f98fe1462625a6c865125fecc9cb6b414f0a5e83#diff-191801bd560e900fbb857aaa11c02aa6R45

---
web3 needs to be beta 33 as per this infura issue
https://github.com/ethereum/web3.js/issues/1559

---
web3 with typescript

https://github.com/ethereum/web3.js/issues/1658#issuecomment-395662469
