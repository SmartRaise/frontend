import * as React from "react";
import { observer, inject } from "mobx-react";
import Slider, { LazyLoadTypes } from "react-slick";

import SliderItem from "./SliderItem";
import MediaQuery from "react-responsive";
import { autorun } from "mobx";
import Spinner from "reactstrap/lib/Spinner";
import Alert from "reactstrap/lib/Alert";
import { projectFullType, projectFullResponse } from "src/web3transactions/transactions/readProjectCore";
import BigNumber from "bignumber.js";

interface ProjectListProps { projectStore; }
export interface ProjectListState { }

@inject("projectStore")
@observer
class ProjectList extends React.Component<ProjectListProps, ProjectListState> {

  constructor(props) {
    super(props)

    // This is a mobX autorun, that makes sure that loadCampaigns is called, when directory changes
    // It's being triggered once on construction, when everything is still empty, (which is why we 
    // need nullcheck)  and later once directory is updated 
    autorun(() => {
      if (this.props.projectStore.directory.size > 0) {
        this.props.projectStore.loadAllCampaigns()
      }
    })
  }

  renderSliderList() {
    return Array.from(this.props.projectStore.projects.values(), (project: projectFullResponse) => {
      let pV = project.contractValues;

      return (
        <SliderItem
          key={pV.contractAddress}
          id={pV.contractAddress}
          current={50}
          title={pV.description.title}
          desc={pV.description.aboutProject}
          image={pV.description.images[0]}
          cntPhases={parseInt(pV.countPhases)}
          phases={pV.phases}
          backers={parseInt(pV.countSupporters)}
          totalCollected={new BigNumber(pV.totalCollected)}
          totalGoal={new BigNumber(pV.totalGoal)}
        />
      );
    });
  }

  render() {

    return (<>
      {this.props.projectStore.directory.size == 0 && !this.props.projectStore.directoryErrors &&
        <div className="load-info">
          <Spinner />Loading directory...
      </div>
      }
      {this.props.projectStore.directoryErrors &&
        <div className="load-info">
          <Alert color="danger">
            {this.props.projectStore.directoryErrors}
          </Alert>
        </div>
      }

      {this.props.projectStore.directory.size > 0 &&
        <div className="slider-projects">
          <MediaQuery maxHeight={699}>
            <Slider {...this.sliderSettings}>{this.renderSliderList()}</Slider>
          </MediaQuery>
          <MediaQuery minHeight={700}>
            {this.renderSliderList()}
          </MediaQuery>
        </div>
      }
    </>);
  }

  type: LazyLoadTypes = "ondemand";
  sliderSettings = {
    arrows: true,
    dots: false,
    infinite: false,
    speed: 400,
    initialSlide: 0,
    centerMode: false,
    centerPadding: "0px",
    variableWidth: true,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
}

export default ProjectList;


