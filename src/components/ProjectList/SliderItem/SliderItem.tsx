import * as React from 'react';
import { Button } from 'reactstrap';
import ReadMoreReact from 'read-more-react';

import PhasesChart from '../../Layout/common/PhasesChart';
import { Link } from 'react-router-dom';

import './SliderItem.scss';
import Infobox from '../../Layout/common/PhasesChart/Infobox';
import BigNumber from 'bignumber.js';

export interface SliderItemProps {
    id: string;
    title: string;
    desc: string;
    image: string;
    current: number;
    cntPhases: number;
    phases: any;
    backers: number;
    totalCollected: BigNumber;
    totalGoal: BigNumber;
}

class SliderItem extends React.Component<SliderItemProps>{
    render() {
        const divStyle = {
            backgroundImage: 'url(' + this.props.image + ')',
        };

        const { cntPhases, backers, totalGoal, totalCollected, title, desc, current, phases } = this.props;

        return (
            <div key={this.props.id} className="slider-item-wrapper">
                <div className="slider-projects__img" style={divStyle}></div>
                <div className="slider-projects__about">
                    <div className="slider-projects__description">
                        <h2 className="slider-projects__title">{title}</h2>
                        <ReadMoreReact text={desc} min={100} ideal={150} max={150} />
                    </div>
                </div>

                <Infobox cntPhases={cntPhases} backers={backers} needed={totalGoal.minus(totalCollected)} />
                <PhasesChart detailsPage={false} current={current} cntPhases={cntPhases} phasesArr={phases} totalGoal={totalGoal} width={300} height={80} />

                <Link to={`/projectDetails/${this.props.id}`} className="slider-projects__read-more">
                    <Button className="read-more">
                        Read More
                   </Button>
                </Link>
            </div>
        );
    }
}

export default SliderItem;
