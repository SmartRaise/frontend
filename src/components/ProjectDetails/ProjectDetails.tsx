import "./ProjectDetails.scss";

import * as React from "react";
import { autorun } from "mobx";
import { observer, inject } from "mobx-react";

import Web3 = require("web3");

import ReadMoreReact from "read-more-react";
import { Link } from "react-router-dom";
import { Row, Col, UncontrolledCollapse, Spinner, Button } from "reactstrap";
import Footer from "../Layout/Footer";

import Milestone from "../Layout/common/Milestone";
import { ProjectStore } from "src/stores/projectStore";
import { FxStore } from "src/web3transactions/fx/fxStore";

import { PhaseType } from "src/web3transactions/transactions/readPhases";

import SendPopup from "../SendPopup/SendPopup";

// TODO: Clean up, seperate props and state, use proper data structures

type ProjectDetailsProps = {
  match,
  projectStore: ProjectStore,
  fxStore: FxStore
}

type ProjectDetailsState = {
  modal: boolean
}

@inject("fxStore", "projectStore")
@observer
class ProjectDetails extends React.Component<ProjectDetailsProps, ProjectDetailsState> {

  constructor(props) {
    super(props)

    // This is a mobX autorun, here we make sure that the campaign is loaded
    // when the directory updates
    autorun(() => {
      if (this.props.projectStore.directory.size > 0) {
        this.props.projectStore.loadAllCampaigns()
      }
    })

    // This is state for the addFunds modal
    this.state = { modal: false }
  }

  toggleSendPopup() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  renderMilestones = (phase: PhaseType, i: number) => {
    let goalEth = parseFloat(Web3.utils.fromWei(phase.goal.toString())).toFixed(2)
    let goalEur = this.props.fxStore.etherToEurRound5(parseFloat(goalEth))
    let displayEur = `${goalEur.toString()} ${this.props.fxStore.fxSymbol}`

    console.log(goalEur)

    return (
      <Milestone
        key={i}
        label={phase.label}
        goal={phase.goal}
        eur={displayEur}
      />
    )
  }

  render() {
    let pro = this.props.projectStore.getCampaign(this.props.match.params.id)
    console.log("Project Details " + JSON.stringify(pro))

    if (!pro) {
      return (<div><Spinner color="primary" /></div>)
    }

    var { title, aboutUs, aboutProject } = pro.contractValues.description;
    var image = pro.contractValues.description.images[0]

    var {
      countPhases,
      countSupporters,
      totalGoal,
      totalCollected,
      phases,
      contractAddress
    } = pro.contractValues

    let externalAuditors = "TBD";
    let legalContact = "TBD";

    const divStyle = { backgroundImage: "url(" + image + ")" };

    const milestones = phases.map((phase, i) => this.renderMilestones(phase, i))

    return (
      <div>
        <div className="section__header-img" style={divStyle} />
        <div className="section section--content">
          <h1 className="section__title">{title}</h1>

          <Row>
            <Col md={6} lg={4}>
              <div className="collapse-block" id="collapse-block">
                <div className="collapse-block__title" id="toggler_aboutus">
                  <img
                    className="collapse-block__icon"
                    src="assets/images/icons/plus.svg"
                    alt="open"
                  />
                  About us
                </div>
                <UncontrolledCollapse
                  className="collapse-block__content"
                  toggler="#toggler_aboutus"
                >
                  <ReadMoreReact
                    text={aboutUs}
                    min={0}
                    ideal={1000}
                    max={1000}
                  />
                </UncontrolledCollapse>
              </div>
            </Col>
            <Col md={6} lg={4}>
              <div className="collapse-block" id="collapse-block">
                <div className="collapse-block__title" id="toggler_project">
                  <img
                    className="collapse-block__icon"
                    src="assets/images/icons/plus.svg"
                    alt="open"
                  />
                  About The Project
                </div>
                <UncontrolledCollapse toggler="#toggler_project">
                  <ReadMoreReact
                    text={aboutProject}
                    min={0}
                    ideal={1000}
                    max={1000}
                  />
                </UncontrolledCollapse>
              </div>
            </Col>
            <Col lg={4}>
              <div className="collapse-block" id="collapse-block">
                <div className="collapse-block__title" id="toggler_milestones">
                  <img
                    className="collapse-block__icon"
                    src="assets/images/icons/plus.svg"
                    alt="open"
                  />
                  Milestones
                </div>
                <UncontrolledCollapse toggler="#toggler_milestones">
                  <>{milestones}</>
                  {/* <NavLink className="read-milestone" href="#">
                    Read Milestone Details
                  </NavLink> */}

                  {/* <Row>
                    <Col sm={{ size: 5, offset: 1 }}>
                      <div className="legal-contact">
                        <p>Legal Contact</p>
                        <span>{legalContact}</span>
                      </div>
                    </Col>
                    <Col sm={{ size: 5, offset: 1 }}>
                      <div className="legal-contact external">
                        <p>External Auditors</p>
                        <span>{externalAuditors}</span>
                      </div>
                    </Col>
                  </Row> */}
                </UncontrolledCollapse>
              </div>
            </Col>
          </Row>
        </div>

        <div className="bottomcover">
          <Link className="go-back" to="/">
            <img
              className="chevron-left"
              src="assets/images/icons/back.svg"
              alt="open"
            />
          </Link>

          <Button className="help-fund text-white" onClick={() => this.toggleSendPopup()}>HELP FUND THE PROJECT</Button>
          <SendPopup isOpen={this.state.modal} toggle={() => this.toggleSendPopup()} recipientAddress={contractAddress} />

          <Footer />
        </div>
      </div >
    );
  }
}

export default ProjectDetails;
