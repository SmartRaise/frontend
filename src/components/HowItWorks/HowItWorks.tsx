import * as React from 'react';
import './HowItWorks.scss';


const HowItWorks = () => {
    return (<>
        <div className="headline"><h1>Smart Raise is an&nbsp;<b>open source</b>,&nbsp;<b>non profit</b>&nbsp;platform for charity fundraising</h1> </div>

        <div className="how-box-wrapper">
            <div className="how-box">
                <h2>Variety of Projects</h2>
                <div className="description">Select form a wide variety of projects and support them to reach their funding goals</div>
                <img src={'assets/images/how/1a.svg'} />
                <img src={'assets/images/how/2.svg'} />
                <img src={'assets/images/how/3.svg'} />
            </div>

            <div className="how-box">
                <h2>Phases</h2>
                <div className="description">Projects have phases, and only get as much money as they need for the phase.</div>
                <img src={'assets/images/how/7.svg'} />
                <img src={'assets/images/how/8a.svg'} />
                <img src={'assets/images/how/9.svg'} />
            </div>

            <div className="how-box">
                <h2>Auditor Approved</h2>
                <div className="description">An auditor decides about payouts. If a project fails to deliver on its goals, the remaining phases get canceled, and the remaining funds are paid back to the supporters.</div>
                <img src={'assets/images/how/4.svg'} />
                <img src={'assets/images/how/6a.svg'} />
                <img src={'assets/images/how/6.svg'} />
            </div>


            <div className="how-box">
                <h2>Smart Contract Enforced</h2>
                <div className="description">All projects are run by smart contracts which enforce the rules. The contract and its source code are public and can be verified by anybody.</div>
                <img className="fullwidth" src={'assets/images/how/10.svg'} />
            </div>

            <div className="FAQ">
                <h1>FAQ</h1>
                <div className="question">How do you make money with this?</div>
                <div className="answer">We don't. The project is orginially funded by the Austrian <a href="http://www.netidee.at"> Netidee</a> grant, and is completely free to use.</div>


                <div className="question">How can I start a campaign?</div>
                <div className="answer">Currently, the system is only wired up for local testing, but this is going to change in the upcoming days. You can set up campaigns in the admin section.</div>


                <div className="question">What is an auditor?</div>
                <div className="answer">An auditor is a knowledgable person who checks the receipts and documentation of a project to verify that they did what they promised to do.</div>

                <div className="question">Who are the auditors?</div>
                <div className="answer">Projects nominate their auditors themselves at the start of the project. Bigger organizations can select professional 3rd party auditors they already work with, smaller organizations can pick a lawyer or type of professional. If you need an auditor, get in touch, we might be able to connect you to pro-bono auditors.</div>

                <div className="question">Why is no the community decision about payouts?</div>
                <div className="answer">There are several problems with crowd-decisions. Firstly, they can be easily gamed. Second, most average people don't have the professional knowledge and time to verifiy this kind of documentation. Thirdly, it would increase complexity by a lot in exchange for little upside. We keep up to date about decentralized voting and curation projects, but for now we focus on simple workflows.</div>

            </div>
            <br/>&nbsp;<br/>&nbsp;<br/>
        </div>
    </>
    );
}

export default HowItWorks;
