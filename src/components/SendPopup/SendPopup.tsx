import "./SendPopup.scss";

import React = require("react");
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Alert } from "reactstrap";
import { Formik, Field, ErrorMessage, FormikBag } from "formik"

import { addFunds } from "../../web3transactions/transactions/addFunds";
import { ValidationError } from "yup";

export interface SendPopupProps {
    isOpen
    toggle,
    recipientAddress
}

export interface SendPopupState {

}

class SendPopup extends React.Component<SendPopupProps, SendPopupState> {
    constructor(props: SendPopupProps) {
        super(props);
        // this.state = { : };
    }

    async onSubmit(x, formikBag) {
        let { setStatus, setSubmitting } = formikBag
        console.log(JSON.stringify(x))
        console.log(JSON.stringify(formikBag))
        console.log(formikBag)
        // debugger

        let res = await addFunds(this.props.recipientAddress, x.sendValue);
        if (res) {
            setStatus({ submitError: null })
            this.props.toggle();
        }
        else {
            setStatus({ submitError: "Your transfer could not be handed over to your ethereum wallet for processing. Can you please verify that you have MetaMask, Mist or similar set up? " })
        }

        setSubmitting(false)
    }

    validate(values) {
        let errors: any = {}

        if (!values.sendValue || isNaN(values.sendValue)) { errors.sendValue = "Please enter how much you'd like to transfer. Only write a number, eg 0.5" }

        return errors
    }

    ValidationError = (props) => {
        return (
            <Alert color="danger">
                {props.children}
            </Alert>
        )
    }

    render() {
        return (
            <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} centered={true} keyboard={true} >
                <Formik
                    initialValues={{ sendValue: '' }}
                    initialStatus={{ submitError: null }}
                    onSubmit={(x, formikBag) => { this.onSubmit(x, formikBag) }}
                    validate={this.validate}
                    render={({ handleSubmit, handleChange, status }) => (
                        <form onSubmit={handleSubmit}>

                            <ModalHeader toggle={this.props.toggle}>Support the project!</ModalHeader>
                            <ModalBody>
                                <Field className="modalInput" name="sendValue" /> 
                                <span className="payPopup__ETH">ETH</span>
                                <ErrorMessage component={this.ValidationError} name="sendValue" />

                                {!!status.submitError ? (
                                    <this.ValidationError> {status.submitError}</this.ValidationError>)
                                    : null}
                            </ModalBody>

                            <ModalFooter>
                                <Button color="secondary" onClick={this.props.toggle}>Cancel</Button>
                                <Button color="primary" type="submit">Send Payment</Button>{' '}
                            </ModalFooter>

                        </form>
                    )}>
                </Formik>
            </Modal >

        );
    }
}

export default SendPopup;