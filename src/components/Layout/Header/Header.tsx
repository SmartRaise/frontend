import * as React from 'react';
import { Link } from 'react-router-dom';
import {Container, Navbar, Row, Col } from 'reactstrap';

import './Header.scss';

interface HeaderProps {
    title: string;
}
interface HeaderState {
    isOpen: boolean;
}

class Header extends React.Component<HeaderProps, HeaderState> {
    constructor(props: any) {
        super(props);

        this.state = {
            isOpen: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        const { title } = this.props;
        return (
           <header className="header">
                <Container fluid>
                    <Row className="align-items-center">
                        <Col>
                            <Link className="navbar-brand header__logo-text" to="/">
                                <img src="assets/images/smartraise.svg" alt="Smart Raise" className="header__img" />
                                SMART&nbsp;<span>RAISE</span>
                            </Link>
                        </Col>
                        <Col>
                            <div className="header__how-it-work">
                                <Link className="nav-link" to="/howItWorks">What is Smart Raise?</Link>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </header>
        );
    }

}

export default Header;
