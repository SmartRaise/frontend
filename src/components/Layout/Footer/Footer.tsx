import * as React from 'react';
import { Link } from 'react-router-dom';
import {
    Container,
    Navbar,
    NavLink,
    Row,
    Col
} from 'reactstrap';

import './Footer.scss';

class Footer extends React.Component {
    render() {
        return (
           <footer className="footer">
                <Container >
                    <Row className="justify-content-center footer__wrap">
                        <Col><NavLink className="footer__link text-center" href="#">Legal Link</NavLink></Col>
                        <Col><NavLink className="footer__link text-center" href="#">Legal Link</NavLink></Col>
                        <Col><NavLink className="footer__link text-center" href="#">Imprint</NavLink></Col>
                    </Row>
                </Container>
            </footer>
        );
    }

}

export default Footer;
