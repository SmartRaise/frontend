import * as React from 'react';
import { Container, Row, Col } from 'reactstrap';


import './PhasesChart.scss';



export  interface PhasesChartState {
    neededInt: any;
    neededAfterDot: any;
}

export  interface PhasesChartProps {
    cntPhases: number;
    current: number;
    backers: number;
    needed: number;
    width: number;
    height: number;
    detailsPage: boolean;
    phasesArr: any;
}


class PhasesChart extends React.Component<PhasesChartProps, PhasesChartState>{

    state = {
        neededInt: '',
        neededAfterDot: ''
    }

    componentWillMount(){
        const num = this.props.needed.toString();;
        const neededInt = Math.floor(this.props.needed);
        const neededAfterDot = num.substr(num.indexOf('.'));
        this.setState({ neededInt, neededAfterDot });
    }

    renderPhases(total: number, width: number, phasesArr: any ){
        const indents = [];
        let phaseLength = Math.round((phasesArr[0].goal/total)*width);

        for (let i = 1; i < phasesArr.length; i++) {
            const animationStyle = { animation: `dash-lines ${i*5}s ease alternate` };
            if(i !== 1 ) phaseLength += Math.round((phasesArr[i-1].goal/total)*width);

            indents.push(
                <g fill="transparent" stroke="#A63442" key={i}>
                    <path className="path-line" style={animationStyle} d={`M${phaseLength} 00 ${phaseLength} 80`}/>
                </g>
            );
        }
        return indents;
    }

    // TODO: Throw out, do new
    renderCurrentPoint(width: number, currentPhases: number, phasesArr: any, total: number){
        let xPosition, maxYText, curentPriceText ;
        if(currentPhases === 1) {
             xPosition = 20;
        }else {

             let donated: number = 0;
            for (let i = 0; i < phasesArr.length; i++) {
                if(currentPhases >= phasesArr[i].phase ){
                    donated += phasesArr[i].goal
                }
                else {
                    break;
                }
            }

            xPosition = Math.round((donated/51000)*width)+20 ;
        }

        let yPoint = Math.round((currentPhases/51000)*100) ;
        if(yPoint < 40) yPoint = 40;
        let minYPoint = (phasesArr[0].goal/total*100 > phasesArr[phasesArr.length-1].goal/total*100) ?  phasesArr[phasesArr.length-1].goal/total*100 : phasesArr[0].goal/total*100;
        const maxXpoint = (xPosition > 285) ? 250 : xPosition;

        const currentPhasePrice = phasesArr.find( (phase: any) => phase.phase == currentPhases ).goal;


        const neededFirst = currentPhasePrice.toString().slice(0, -3);
        const neededSecond = currentPhasePrice.toString().slice(-3);
        //when 2 phases and current phase is 2
        if(xPosition > 250){
            maxYText = 10;
            curentPriceText = (
                <g>
                    <text x={(neededFirst.length === 1) ? maxXpoint-65: maxXpoint-73} y={maxYText+35} fontSize="18" fill="#A63442" fontWeight={500}>{neededFirst}</text>
                    <text x={maxXpoint-55} y={maxYText+35} fontSize="12" fill="#A63442">,{neededSecond} €</text>
                </g>
            );
        }else {
           maxYText = minYPoint;
            curentPriceText = (
                <g>
                    <text x={(neededFirst.length === 1) ? maxXpoint-10 : maxXpoint-18} y={maxYText+35} fontSize="18" fill="#A63442" fontWeight={500}>{neededFirst}</text>
                    <text x={maxXpoint} y={maxYText+35} fontSize="12" fill="#A63442">,{neededSecond} €</text>
                </g>
            )
        }

        if(minYPoint > yPoint) {
            minYPoint = yPoint;
        }


        return (
            <g>
                {curentPriceText}
                <circle cx={maxXpoint} cy={minYPoint} r="6" fill="transparent" stroke="#A63442" strokeWidth="3"/>
                <circle cx={maxXpoint} cy={minYPoint} r="11" fill="transparent" stroke="#A63442" strokeWidth="1" opacity="0.5"/>
                <circle cx={maxXpoint} cy={minYPoint} r="16" fill="transparent" stroke="#A63442" strokeWidth="1" opacity="0.25"/>
            </g>

        );
    }

    renderNeededmoney(total: number, phasesArr: any){
        const y = (phasesArr[phasesArr.length-1].goal/total)*100 + 10;
        return (
            <g fill="#A63442">
                <text x={245} y={y} fontSize="18"  fontWeight={500}>53</text>
                <text x={263} y={y} fontSize="12" >,500 €</text>
            </g>
        )
    }


    renderChartCurve(phasesArr: any){
        const total = 51000;
        let  y ,y5;
        y = phasesArr[0].goal/total*100;
        y5 = phasesArr[phasesArr.length-1].goal/total*100;

        return (
            <path className="path" d={`M0 ${y} T 60 20 T 285 ${y5}`} stroke="black" fill="transparent"/>
        );
    }

    renderFlag(width: number, total: number, phasesArr: any){
        return (
            <image x={width-20} y={(phasesArr[phasesArr.length-1].goal/total)*100-20}  href="assets/images/icons/flag.svg" height="20" width="20"/>
        );
    }

    renderChart(detailsPage: boolean){
        const { cntPhases, current, phasesArr } = this.props;

        if(detailsPage){
            return(
                <div className="graph-details">
                    <svg width="100%" height="150" xmlns="http://www.w3.org/2000/svg">
                        <path className="path" d="M0 100 C 60 40, 75 30, 125 80 S 150 150, 300 40" stroke="black" fill="transparent"/>
                    </svg>
                </div>
            );
        }
        // {this.renderCurrentPoint(300, current, phasesArr, 51000)}

        return (
            <div>
                <svg width="300" height="80" xmlns="http://www.w3.org/2000/svg">
                    {this.renderChartCurve(phasesArr)}
                    {this.renderPhases(51000, 300, phasesArr)}
                    {this.renderNeededmoney(51000, phasesArr)}
                    {this.renderFlag(300, 51000, phasesArr )}
                </svg>
            </div>
        );

    }

    render() {
        const { cntPhases, backers, detailsPage } = this.props;
        const { neededInt, neededAfterDot } = this.state;
        const divStyle = { stroke: 'rgb(255,0,0)', strokeWidth: 2 };

        return (
            <div className="phases-chart">
                <div className="phases-chart__info">
                    <Container>
                        <Row>
                            <Col>
                                <div className="d-flex align-items-baseline">
                                    <span className="phases-chart__text">{cntPhases}</span>&nbsp;
                                    <span>Phase</span>
                                </div>
                            </Col>
                            <Col>
                                <div className="d-flex align-items-baseline">
                                    <span className="phases-chart__text">{backers}</span>&nbsp;
                                    <span>Backers</span>
                                </div>
                            </Col>
                            <Col>
                                <div className="still-needed">
                                    <div className="d-flex align-items-baseline">
                                        <span className="phases-chart__text needed-int">{neededInt}</span>
                                        <span className="phases-chart__text">{neededAfterDot} ETH</span>
                                    </div>
                                    <div className="text-nowrap">Still Needed</div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <div className="phases-chart__graph">
                    {this.renderChart(detailsPage) }
                </div>
            </div>

        );
    }
}

export default PhasesChart;
