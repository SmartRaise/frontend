import * as React from 'react';
import BigNumber from "bignumber.js"

import './PhasesChart.scss';
import { PhaseType } from 'src/web3transactions/transactions/readPhases';

export interface PhasesChartState {
}

export interface PhasesChartProps {
    cntPhases: number;
    current: number;
    totalGoal: BigNumber;
    width: number;
    height: number;
    detailsPage: boolean;
    phasesArr: Array<PhaseType>;
}

class PhasesChart extends React.Component<PhasesChartProps, PhasesChartState>{

    phaseCoordinates: Array<{ x: number, y: number }> = [];

    calculatePhaseCoordinates() {
        const lastPhase = this.props.phasesArr.slice(-1)[0]
        const lastDate = lastPhase.dueDate;
        const totalDuration = lastDate - this.props.phasesArr[0].dueDate;

        this.phaseCoordinates = this.props.phasesArr.map((el, i, arr) => {
            return {
                x: (el.dueDate - arr[0].dueDate) / totalDuration * this.props.width * 0.7 + 50,
                y: this.props.height - (el.accumulatedGoal.div(this.props.totalGoal).toNumber() * (this.props.height - 20))
            }
        })
    }

    renderPhaseLines() {
        var lines = this.phaseCoordinates.map((el, i) => {
            let animationStyle = { animation: `dash-lines ${i * 5}s ease alternate` };
            return (<path key={i} className="path-line" style={animationStyle} d={`M${el.x} 00 ${el.x} 80`} />)
        })

        return (
            < g fill="transparent" stroke="#A63442">
                {lines}
            </g >
        )
    }

    renderChartCurve() {
        var segments = this.phaseCoordinates.map((el, i) => {
            return ` L ${el.x} ${el.y}`
        })

        segments.push(` L ${this.props.width - 20} ${this.phaseCoordinates[this.phaseCoordinates.length - 1].y}`)

        let dvar = `M 0 ${this.props.height}` + segments.join(" ");

        return (
            <path className="path" d={dvar} stroke="#323d4f" fill="transparent" />
        )


    }

    renderTextFundingGoal(total: number, phasesArr) {
        const y = (phasesArr[phasesArr.length - 1].goal / total) * 100 + 10;
        return (
            <g fill="#A63442">
                <text x={245} y={y} fontSize="18" fontWeight={500}>53</text>
                <text x={263} y={y} fontSize="12" >,500 €</text>
            </g>
        )
    }

    renderFlag() {
        return (
            <image x={this.props.width - 20} y={this.phaseCoordinates[this.phaseCoordinates.length - 1].y - 20}
                href="assets/images/icons/flag.svg" height="20" width="20" />
        );
    }

    renderChart(detailsPage: boolean) {

        const { cntPhases, current, phasesArr } = this.props;

        // if(detailsPage){
        //     return(
        //         <div className="graph-details">
        //             <svg width="100%" height="150" xmlns="http://www.w3.org/2000/svg">
        //                 <path className="path" d="M0 100 C 60 40, 75 30, 125 80 S 150 150, 300 40" stroke="black" fill="transparent"/>
        //             </svg>
        //         </div>
        //     );
        // }

        return (
            <div>
                <svg width="300" height="80" xmlns="http://www.w3.org/2000/svg">
                    {this.renderChartCurve()}
                    {this.renderPhaseLines()}
                    {/* {this.renderTextFundingGoal(this.props.totalGoal, phasesArr)} */}
                    {/* {this.renderCurrentPoint(300, current, phasesArr, 51000)} */}
                    {this.renderFlag()}
                </svg>
            </div>
        );

    }

    render() {

        this.calculatePhaseCoordinates();

        const { cntPhases, detailsPage } = this.props;
        const divStyle = { stroke: 'rgb(255,0,0)', strokeWidth: 2 };

        return (
            <div className="phases-chart">
                <div className="phases-chart__graph">
                    {this.renderChart(detailsPage)}
                </div>
            </div>

        );
    }
}

export default PhasesChart;


    // TODO: Throw out, do new
    // renderCurrentPoint(width: number, currentPhases: number, phasesArr: any, total: number) {
    //     let xPosition, maxYText, curentPriceText;
    //     if (currentPhases === 1) {
    //         xPosition = 20;
    //     } else {

    //         let donated: number = 0;
    //         for (let i = 0; i < phasesArr.length; i++) {
    //             if (currentPhases >= phasesArr[i].phase) {
    //                 donated += phasesArr[i].goal
    //             }
    //             else {
    //                 break;
    //             }
    //         }

    //         xPosition = Math.round((donated / 51000) * width) + 20;
    //     }

    //     let yPoint = Math.round((currentPhases / 51000) * 100);
    //     if (yPoint < 40) yPoint = 40;
    //     let minYPoint = (phasesArr[0].goal / total * 100 > phasesArr[phasesArr.length - 1].goal / total * 100) ? phasesArr[phasesArr.length - 1].goal / total * 100 : phasesArr[0].goal / total * 100;
    //     const maxXpoint = (xPosition > 285) ? 250 : xPosition;

    //     const currentPhasePrice = phasesArr.find((phase: any) => phase.phase == currentPhases).goal;


    //     const neededFirst = currentPhasePrice.toString().slice(0, -3);
    //     const neededSecond = currentPhasePrice.toString().slice(-3);
    //     //when 2 phases and current phase is 2
    //     if (xPosition > 250) {
    //         maxYText = 10;
    //         curentPriceText = (
    //             <g>
    //                 <text x={(neededFirst.length === 1) ? maxXpoint - 65 : maxXpoint - 73} y={maxYText + 35} fontSize="18" fill="#A63442" fontWeight={500}>{neededFirst}</text>
    //                 <text x={maxXpoint - 55} y={maxYText + 35} fontSize="12" fill="#A63442">,{neededSecond} €</text>
    //             </g>
    //         );
    //     } else {
    //         maxYText = minYPoint;
    //         curentPriceText = (
    //             <g>
    //                 <text x={(neededFirst.length === 1) ? maxXpoint - 10 : maxXpoint - 18} y={maxYText + 35} fontSize="18" fill="#A63442" fontWeight={500}>{neededFirst}</text>
    //                 <text x={maxXpoint} y={maxYText + 35} fontSize="12" fill="#A63442">,{neededSecond} €</text>
    //             </g>
    //         )
    //     }

    //     if (minYPoint > yPoint) {
    //         minYPoint = yPoint;
    //     }


    //     return (
    //         <g>
    //             {curentPriceText}
    //             <circle cx={maxXpoint} cy={minYPoint} r="6" fill="transparent" stroke="#A63442" strokeWidth="3" />
    //             <circle cx={maxXpoint} cy={minYPoint} r="11" fill="transparent" stroke="#A63442" strokeWidth="1" opacity="0.5" />
    //             <circle cx={maxXpoint} cy={minYPoint} r="16" fill="transparent" stroke="#A63442" strokeWidth="1" opacity="0.25" />
    //         </g>

    //     );
    // }

