import * as React from 'react';
import { Container, Row, Col } from 'reactstrap';
import Web3 = require("web3");



export default function Infobox({ cntPhases, backers, needed }) {
    let [counter, decimal] = parseFloat(Web3.utils.fromWei(needed.toString(), "ether")).toFixed(1).split('.');

    return (
        <div className="phases-chart__info">
            <Container>
                <Row>
                    <Col>
                        <div className="d-flex align-items-baseline">
                            <span className="phases-chart__text">{cntPhases}</span>&nbsp;
                    <span>Phases</span>
                        </div>
                    </Col>
                    {/* We don't show backers until we got some traffic on the page
                     <Col>
                        <div className="d-flex align-items-baseline">
                            <span className="phases-chart__text">{backers}</span>&nbsp;
                    <span>Backers</span>
                        </div>
                    </Col> */}
                    <Col>
                        <div className="still-needed">
                            <div className="d-flex align-items-baseline">
                                <span className="phases-chart__text needed-int">{counter}</span>
                                <span className="phases-chart__text">.{decimal} ETH</span>
                            </div>
                            <div className="text-nowrap">Still Needed</div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
