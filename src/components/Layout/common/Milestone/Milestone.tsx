import * as React from 'react';
import Web3 = require("web3");

import './Milestone.scss';
import BigNumber from 'bignumber.js';

export interface MilestoneProps {
    label: string;
    goal: BigNumber;
    eur: string;
}

class Milestone extends React.Component<MilestoneProps> {
    render() {
        const { label: text, goal, eur } = this.props;

        const priceInEth = parseFloat(Web3.utils.fromWei(goal.toString())).toFixed(2);

        return (
            <div className="milestone-wrap">
                <span className="milestone-wrap__title">{text}</span>
                <p className="milestone-wrap__text-wrap">
                    <div className="milestone-wrap__text info-eth">{priceInEth} ETH </div>
                    <div className="milestone-wrap__text info-eur">~ {eur}</div>
                </p>

            </div>
        );
    }
}

export default Milestone;