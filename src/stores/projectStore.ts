import { observable, action } from 'mobx';
import { dummyProjects } from './dummyProjects';

import { readDirectory } from '../web3transactions/transactions/readDirectory'
import { readProjectCore, projectFullResponse } from '../web3transactions/transactions/readProjectCore'

export class ProjectStore {

    @observable projects: Map<string, projectFullResponse> = new Map();
    @observable directory: Set<string> = new Set([]);
    @observable directoryErrors: string | null = null;

    web3;

    constructor(web3) {
        this.web3 = web3;

        // this.projects = dummyProjects;
        this.loadDirectory();   // Trigger, we don't wait
    }


    getCampaign(address: string) {

        // If it's not yet loaded, let's load it. Once it's loaded, mobx triggers a redraw on the observer
        // Here we could be more sophisticated and implement a simple timeouts / caching mechanism against
        // stale data
        if (!this.projects.get(address)) {
            this.loadCampaign(address)
        }

        return this.projects.get(address);
    }

    // Directory addresses
    // srdev 0xCD8bCfa5c383Abbd368c83fD6f2DADEbe2844c71
    // ropsten 0xBb90ff9a64bD484B2EDB861A95c27D8918b2bb0f
    // ropsten new  0xee715b26c2432198f55872ea9e12c6108b69d88d
    // srdev new 0x1eCa7782a035B7c6A037358Afac3223010DFb151

    @action
    async loadDirectory() {
        let bn = await this.web3.eth.getBlockNumber(); // This is for debugging purposes
        console.log(bn)

        let res = await readDirectory(this.web3, '0x1eCa7782a035B7c6A037358Afac3223010DFb151')
        
        if (res.contractErrors) { this.directoryErrors = res.contractErrors }
        if (res.contractValues && res.contractValues.directory) { this.directory = new Set(res.contractValues.directory) }

        console.log("Loaded Directory")
    }

    async loadAllCampaigns() {
        console.log("loadCampaigns")

        this.directory.forEach((address) => this.loadCampaign(address))
    }

    @action
    async loadCampaign(campaignAddress) {

        // We ONLY load contracts that are listed in the directory, to avoid showing arbitrary 
        // data in our frame.(XSS) This check could be made optional in settings or replaced by a warning.
        if (this.directory.has(campaignAddress)) {
            var campResults = await readProjectCore(this.web3, campaignAddress);
            this.projects.set(campaignAddress, campResults)
        }
    }
}