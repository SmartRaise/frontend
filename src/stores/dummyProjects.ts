export const dummyProjects =
    [
        {
            id: 1,
            phases: [
                { phase: "1", needed: 15000 },
                { phase: "2", needed: 6000 },
                { phase: "3", needed: 30000 }
            ],
            projectTitle:
                "Bitte helft uns, damit wir helfen können! Unsere Obdachlosenhilfe e.V.",
            projectDescription:
                "Als die Obdachlosenhilfe e.V. organisieren wir dreimal in der Woche Hilfstouren zum Hauptplatz.Dort versorgen wir bedürftige Menschen mit einer warmen Mahlzeit, Kleidung und Dingen des alltäglichen Bedarfs. Neben dieser materiellen Hilfe wollen wir durch Gespräche und Vertrauen die Lebenssituation der Menschen nachhaltig verbessen. Manchmal hilft schon ein offenes Ohr, doch auf Wunsch hin beraten wir unsere Gäste auch und  vermitteln sie zu geeigneten Stellen. Wir treten für ein solidarisches Miteinander und die Akzeptanz aller Lebensformen ein. Durch verstärkte Öffentlichkeitsarbeit soll diese Position auch der Gesellschaft nähergebracht werden. ",
            aboutUs:
                "Als die Obdachlosenhilfe e.V. organisieren wir dreimal in der Woche Hilfstouren zum Hauptplatz.Dort versorgen wir bedürftige Menschen mit einer warmen Mahlzeit, Kleidung und Dingen des alltäglichen Bedarfs. Neben dieser materiellen Hilfe wollen wir durch Gespräche und Vertrauen die Lebenssituation der Menschen nachhaltig verbessen. Manchmal hilft schon ein offenes Ohr, doch auf Wunsch hin beraten wir unsere Gäste auch und  vermitteln sie zu geeigneten Stellen. Wir treten für ein solidarisches Miteinander und die Akzeptanz aller Lebensformen ein. Durch verstärkte Öffentlichkeitsarbeit soll diese Position auch der Gesellschaft nähergebracht werden. ",
            src: "assets/images/171211-F-IP259-015.JPG",
            phase: 3,
            backers: 5,
            needed: 10.5,
            current: 1
        },
        {
            id: 2,
            phases: [
                { phase: "1", needed: 20000 },
                { phase: "2", needed: 6000 },
                { phase: "3", needed: 10000 },
                { phase: "4", needed: 15000 }
            ],
            projectTitle: "Schluss mit Tierquälerei!",
            projectDescription:
                "Das Treiben der Fleisch- und Milchindustrie ist schockierend. Auch das der Pharma-, der Kosmetik und das der Modeindustrie: Überall auf der Welt ist es zur millionenfachen Gewohnheit geworden: Unschuldigen Tieren ihr Leben nehmen. 1933 wurde das Tierschutzgesetz verabschiedet und immer wieder angepasst. Heute, 85 Jahre später schlachten wir jährlich 800 Millionen unschuldige Tiere ab und quälen Tiere mit tödlichen Experimenten – und das alles ist völlig unnötig. Man hört immer wieder „die Politik soll es richten“. Nachdem wir nun schon 85 Jahre darauf warten, stellt sich die Frage, wie lange noch? ",
            aboutUs:
                "Das Treiben der Fleisch- und Milchindustrie ist schockierend. Auch das der Pharma-, der Kosmetik und das der Modeindustrie: Überall auf der Welt ist es zur millionenfachen Gewohnheit geworden: Unschuldigen Tieren ihr Leben nehmen. 1933 wurde das Tierschutzgesetz verabschiedet und immer wieder angepasst. Heute, 85 Jahre später schlachten wir jährlich 800 Millionen unschuldige Tiere ab und quälen Tiere mit tödlichen Experimenten – und das alles ist völlig unnötig. Man hört immer wieder „die Politik soll es richten“. Nachdem wir nun schon 85 Jahre darauf warten, stellt sich die Frage, wie lange noch? ",
            src: "assets/images/1280px-Holstein_dairy_cows.jpg",
            phase: 4,
            backers: 12,
            needed: 21.5,
            current: 3
        },
        {
            id: 3,
            phases: [{ phase: "1", needed: 25000 }, { phase: "2", needed: 26000 }],
            projectTitle: " Hungersnot in Afrika | Aktion Deutschland Hilft",
            projectDescription:
                "Durch die extreme Dürre am Horn von Afrika und anhaltende Gewalt in der Region um den Tschadsee leiden Millionen Menschen unter Hunger. Unterstütze die notleidenden Menschen mit Deiner Spende!",
            aboutUs:
                "Das Treiben der Fleisch- und Milchindustrie ist schockierend. Auch das der Pharma-, der Kosmetik und das der Modeindustrie: Überall auf der Welt ist es zur millionenfachen Gewohnheit geworden: Unschuldigen Tieren ihr Leben nehmen. 1933 wurde das Tierschutzgesetz verabschiedet und immer wieder angepasst. Heute, 85 Jahre später schlachten wir jährlich 800 Millionen unschuldige Tiere ab und quälen Tiere mit tödlichen Experimenten – und das alles ist völlig unnötig. Man hört immer wieder „die Politik soll es richten“. Nachdem wir nun schon 85 Jahre darauf warten, stellt sich die Frage, wie lange noch? ",
            src: "assets/images/Children_in_Somalia.JPG",
            phase: 2,
            backers: 10,
            needed: 18.5,
            current: 2
        },
        {
            id: 4,
            phases: [
                { phase: "1", needed: 10000 },
                { phase: "2", needed: 6000 },
                { phase: "3", needed: 10000 },
                { phase: "4", needed: 5000 },
                { phase: "5", needed: 20000 }
            ],
            projectTitle:
                "Dringende Rettungsaktion: Fast 10.000 Schildkröten brauchen Hilfe!",
            aboutUs:
                "Das Treiben der Fleisch- und Milchindustrie ist schockierend. Auch das der Pharma-, der Kosmetik und das der Modeindustrie: Überall auf der Welt ist es zur millionenfachen Gewohnheit geworden: Unschuldigen Tieren ihr Leben nehmen. 1933 wurde das Tierschutzgesetz verabschiedet und immer wieder angepasst. Heute, 85 Jahre später schlachten wir jährlich 800 Millionen unschuldige Tiere ab und quälen Tiere mit tödlichen Experimenten – und das alles ist völlig unnötig. Man hört immer wieder „die Politik soll es richten“. Nachdem wir nun schon 85 Jahre darauf warten, stellt sich die Frage, wie lange noch? ",
            projectDescription:
                "+++Aktuelle Rettungsaktion: Tausende Schildkröten brauchen dringend Hilfe - Auf Madagaskar wurden fast 11.000 (!) Schildkröten beschlagnahmt. In einer solchen Situation ist schnelle, unbürokratische Hilfe gefragt. Bitte hilf mit einer Spende diese Schildkröten zu retten. Mehr zu dieser Rettungsaktion+++",
            src: "assets/images/Green_turtle_swimming_over_coral_reefs_in_Kona.jpg",
            phase: 5,
            backers: 20,
            needed: 52.5,
            current: 2
        },
        {
            id: 5,
            phases: [
                { phase: "1", needed: 20000 },
                { phase: "2", needed: 6000 },
                { phase: "3", needed: 10000 },
                { phase: "4", needed: 15000 }
            ],
            projectTitle: "Refugee crisis: Emergency appeal",
            projectDescription:
                "Millions of people have been forced to flee their homes, to run for their lives and risk the safety of their families.Oxfam urgently need to help more people in Syria, Jordan, Lebanon and closer to home in Europe, too.You can help.",
            aboutUs:
                "Als die Obdachlosenhilfe e.V. organisieren wir dreimal in der Woche Hilfstouren zum Hauptplatz.Dort versorgen wir bedürftige Menschen mit einer warmen Mahlzeit, Kleidung und Dingen des alltäglichen Bedarfs. Neben dieser materiellen Hilfe wollen wir durch Gespräche und Vertrauen die Lebenssituation der Menschen nachhaltig verbessen. Manchmal hilft schon ein offenes Ohr, doch auf Wunsch hin beraten wir unsere Gäste auch und  vermitteln sie zu geeigneten Stellen. Wir treten für ein solidarisches Miteinander und die Akzeptanz aller Lebensformen ein. Durch verstärkte Öffentlichkeitsarbeit soll diese Position auch der Gesellschaft nähergebracht werden. ",
            src: "assets/images/kitali-refugee-camp.jpg",
            phase: 4,
            backers: 5,
            needed: 21.5,
            current: 2
        },
        {
            id: 6,
            phases: [
                { phase: "1", needed: 25000 },
                { phase: "2", needed: 6000 },
                { phase: "3", needed: 30000 }
            ],
            projectTitle: "Kanduyi children education 2",
            projectDescription: "Lorem Ipsum is simply dummy text ",
            aboutUs:
                "Als die Obdachlosenhilfe e.V. organisieren wir dreimal in der Woche Hilfstouren zum Hauptplatz.Dort versorgen wir bedürftige Menschen mit einer warmen Mahlzeit, Kleidung und Dingen des alltäglichen Bedarfs. Neben dieser materiellen Hilfe wollen wir durch Gespräche und Vertrauen die Lebenssituation der Menschen nachhaltig verbessen. Manchmal hilft schon ein offenes Ohr, doch auf Wunsch hin beraten wir unsere Gäste auch und  vermitteln sie zu geeigneten Stellen. Wir treten für ein solidarisches Miteinander und die Akzeptanz aller Lebensformen ein. Durch verstärkte Öffentlichkeitsarbeit soll diese Position auch der Gesellschaft nähergebracht werden. ",
            src: "assets/images/children-817365_1920.jpg",
            phase: 3,
            backers: 5,
            needed: 10.5,
            current: 2
        }
    ]