import * as React from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';

import {
    Container
} from 'reactstrap';
import Header from './components/Layout/Header';
import Footer from './components/Layout/Footer';
import ProjectList from './components/ProjectList';
import HowItWorks from './components/HowItWorks';
import ProjectDetails from './components/ProjectDetails';

import './scss/App.scss';

type AppProps = {}

class App extends React.Component<AppProps> {
    constructor(props: any) {
        super(props);
    }
    render() {
        return (
            <div className="page-wrapper">
                <Header title="Smart Raise" />
                <main className="main">
                    <Container fluid>
                        <Switch>
                            <Route exact path="/" component={ProjectList}></Route>
                            <Route path="/howItWorks" component={HowItWorks}></Route>
                            <Route path="/projectDetails/:id" component={ProjectDetails}></Route>
                        </Switch>
                    </Container>
                </main>
                <Footer />
            </div>
        );
    }
}

export default withRouter(App);
