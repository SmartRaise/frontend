import * as React from "react";
import * as ReactDOM from "react-dom";
import * as MobXReact from "mobx-react";
import { HashRouter } from 'react-router-dom';
import { ScrollContext } from 'react-router-scroll-4';
import Web3 = require("web3");
import { ProjectStore } from '../src/stores/projectStore';
import { FxStore } from "../src/web3transactions/fx/fxStore";

// const web3 = new Web3("ws://localhost:8545");
const web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/dec707a3b05e46f18f4f8e521c31726b"));
// const web3 = new Web3("ws://ropsten.infura.io:8545");
// const web3 = new Web3(new Web3.providers.WebsocketProvider("wss://ropsten.infura.io/ws/v3/dec707a3b05e46f18f4f8e521c31726b"))
// const web3 = new Web3("ws://localhost:8545");

console.log("Web3 version: " + web3.version)

const projectStoreInst = new ProjectStore(web3);
const fxStoreInst = new FxStore();
console.log(fxStoreInst)

import App from './App';

ReactDOM.render(
    <MobXReact.Provider projectStore={projectStoreInst} fxStore={fxStoreInst}>
        <HashRouter><ScrollContext>
            <App />
        </ScrollContext></HashRouter>
    </MobXReact.Provider>
    , document.getElementById("root")
);